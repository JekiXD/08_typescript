import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter, FighterDetails, fightersDetails } from '../helpers/mockData';

export  function showFighterDetailsModal(fighter : FighterDetails) : void {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter : FighterDetails) : HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const fighterinfo = createElement({ tagName: 'div', className: 'fighter-info' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src : fighter.source}});

  nameElement.innerText = fighter.name;
  attackElement.innerText = "Attack: " + fighter.attack.toString();
  defenseElement.innerText = "Defense: " + fighter.defense.toString();
  healthElement.innerText = "Health: " + fighter.health.toString();

  fighterinfo.append(nameElement, attackElement, defenseElement, healthElement)
  fighterDetails.append(fighterinfo,imageElement);

  return fighterDetails;
}
